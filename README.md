# Report Service
[![pipeline status](https://gitlab.com/adpro-b5/report-service/badges/master/pipeline.svg)](https://gitlab.com/adpro-b5/report-service/-/commits/master)
[![coverage report](https://gitlab.com/adpro-b5/report-service/badges/master/coverage.svg)](https://gitlab.com/adpro-b5/report-service/-/commits/master)

Deployed on https://charitable-report.herokuapp.com