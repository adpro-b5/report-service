package com.charitable.report.controller;

import com.charitable.report.service.ReportService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(controllers = ReportController.class)
class ReportControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ReportService reportService;
    @InjectMocks
    private ReportController underTest;

    @Test
    void whenReportURLIsCalled() throws Exception {
        mockMvc.perform(get("/all"))
                .andExpect(status().isOk());
    }

    @Nested
    class WhenGettingReport {
        @BeforeEach
        void setup() {
        }
    }

    @Nested
    class WhenNotifyingReport {
        private final int DONATION_AMOUNT = 25;
        @Mock
        private String type;

        @BeforeEach
        void setup() {
        }
    }
}