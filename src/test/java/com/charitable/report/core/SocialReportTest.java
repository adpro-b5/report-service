package com.charitable.report.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SocialReportTest {

    private Class<?> socialReportClass;
    private SocialReport socialReport;

    @Mock
    private ReportObserver reportObserver;

    @BeforeEach
    public void setUp() throws Exception {
        socialReportClass = Class.forName("com.charitable.report.core.SocialReport");
        socialReport = new SocialReport(reportObserver);
    }

    @Test
    void testSocialReportIsConcreteClass() {
        assertFalse(Modifier.isAbstract(socialReportClass.getModifiers()));
    }

    @Test
    void testSocialReportIsAReport() {
        Class<?> parentClass = socialReportClass.getSuperclass();
        assertEquals("com.charitable.report.core.Report", parentClass.getName());
    }

    @Test
    void whenUpdateMethodIsCalled() {
        when(reportObserver.getType()).thenReturn("Social");
        when(reportObserver.getDonationAmount()).thenReturn(100);

        socialReport.update();
        assertEquals(100, socialReport.getTotalDonation());
    }

    @Test
    void TestGetterMethod() {
        assertEquals("Social Campaign", socialReport.getName());
        assertEquals("/campaign/social.png", socialReport.getImage());
    }
}
