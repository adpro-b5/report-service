package com.charitable.report.core;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ReportObserverTest {

    private static ReportObserver reportObserver;
    private static EducationalReport educationalReport;
    private static HealthReport healthReport;

    @BeforeAll
    static void setUp() {
        reportObserver = new ReportObserver();
        educationalReport = new EducationalReport(reportObserver);
        healthReport = new HealthReport(reportObserver);

        reportObserver.add(educationalReport);
        reportObserver.add(healthReport);
    }

    @Test
    void testWhenAddDonationIsCalledItShouldSetTheTypeAndAmountProperty() {
        reportObserver.addDonation("Donation", 100);
        assertEquals("Donation", reportObserver.getType());
        assertEquals(100, reportObserver.getDonationAmount());
    }

    @Test
    void testGetListOfReportsReturnCorrectList() {
        List<Report> expected = Arrays.asList(educationalReport, healthReport);
        assertThat(reportObserver.getListOfReports(), is(expected));
    }
}
