package com.charitable.report.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class EducationalReportTest {

    private Class<?> educationalReportClass;
    private EducationalReport educationalReport;

    @Mock
    private ReportObserver reportObserver;

    @BeforeEach
    public void setUp() throws Exception {
        educationalReportClass = Class.forName("com.charitable.report.core.EducationalReport");
        educationalReport = new EducationalReport(reportObserver);
    }

    @Test
    void testEducationalReportIsConcreteClass() {
        assertFalse(Modifier.isAbstract(educationalReportClass.getModifiers()));
    }

    @Test
    void testEducationalReportIsAReport() {
        Class<?> parentClass = educationalReportClass.getSuperclass();
        assertEquals("com.charitable.report.core.Report", parentClass.getName());
    }

    @Test
    void whenUpdateMethodIsCalled() {
        when(reportObserver.getType()).thenReturn("Educational");
        when(reportObserver.getDonationAmount()).thenReturn(100);

        educationalReport.update();
        assertEquals(100, educationalReport.getTotalDonation());
    }

    @Test
    void TestGetterMethod() {
        assertEquals("Educational Campaign", educationalReport.getName());
        assertEquals("/campaign/edu.png", educationalReport.getImage());
    }

}