package com.charitable.report.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class HealthReportTest {

    private Class<?> healthReportClass;
    private HealthReport healthReport;

    @Mock
    private ReportObserver reportObserver;

    @BeforeEach
    public void setUp() throws Exception {
        healthReportClass = Class.forName("com.charitable.report.core.HealthReport");
        healthReport = new HealthReport(reportObserver);
    }

    @Test
    void testHealthReportIsConcreteClass() {
        assertFalse(Modifier.isAbstract(healthReportClass.getModifiers()));
    }

    @Test
    void testHealthReportIsAReport() {
        Class<?> parentClass = healthReportClass.getSuperclass();
        assertEquals("com.charitable.report.core.Report", parentClass.getName());
    }

    @Test
    void whenUpdateMethodIsCalled() {
        when(reportObserver.getType()).thenReturn("Health");
        when(reportObserver.getDonationAmount()).thenReturn(100);

        healthReport.update();
        assertEquals(100, healthReport.getTotalDonation());
    }

    @Test
    void TestGetterMethod() {
        assertEquals("Health Campaign", healthReport.getName());
        assertEquals("/campaign/health.png", healthReport.getImage());
    }

}
