package com.charitable.report.service;

import com.charitable.report.core.Report;
import com.charitable.report.core.ReportObserver;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReportServiceImplTest {

    @InjectMocks
    ReportServiceImpl reportServiceImpl;
    @Mock
    private ReportObserver reportObserver;
    @InjectMocks
    private ReportServiceImpl underTest;

    @BeforeAll
    public static void setUp() {
//        reportServiceImpl = new ReportServiceImpl();
    }

    @Test
    void whenUpdateReportMethodIsCalled() {
//        reportServiceImpl.updateReport("Educational", 100);
//
//        List<Report> listOfReport = reportServiceImpl.getReport();
//        for (Report report : listOfReport) {
//            if (report.getName().equals("Educational")) {
//                assertEquals(100, report.getTotalDonation());
//            }
//        }
    }

    @Nested
    class WhenGettingReport {
        @BeforeEach
        void setup() {
        }
    }

    @Nested
    class WhenUpdatingReport {
        private final String TYPE = "TYPE";
        private final int DONATION_AMOUNT = 87;

        @BeforeEach
        void setup() {
        }
    }
}
