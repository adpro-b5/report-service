package com.charitable.report.service;

import com.charitable.report.core.Report;

import java.util.List;

public interface ReportService {

    List<Report> getReport();
    void updateReport(String type, int donationAmount);
}