package com.charitable.report.service;

import com.charitable.report.core.*;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReportServiceImpl implements ReportService {

    private final ReportObserver reportObserver;

    public ReportServiceImpl() {
        reportObserver = new ReportObserver();

        Report educationalReport = new EducationalReport(reportObserver);
        Report socialReport = new SocialReport(reportObserver);
        Report healthReport = new HealthReport(reportObserver);

        reportObserver.add(educationalReport);
        reportObserver.add(socialReport);
        reportObserver.add(healthReport);
    }

    @Override
    public List<Report> getReport() {
        return reportObserver.getListOfReports();
    }

    @Override
    public void updateReport(String type, int donationAmount) {
        reportObserver.addDonation(type, donationAmount);
    }

}
