package com.charitable.report.core;

public class SocialReport extends Report {

    public SocialReport(ReportObserver reportObserver) {
        this.name = "Social Campaign";
        this.reportObserver = reportObserver;
        this.image = "/campaign/social.png";
    }

    @Override
    public void update() {
        String donationType = reportObserver.getType();

        if (donationType.equals("Social")) {
            addTotalDonation(reportObserver.getDonationAmount());
        }
    }
}
