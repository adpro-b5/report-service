package com.charitable.report.core;

public class EducationalReport extends Report  {

    public EducationalReport(ReportObserver reportObserver) {
        this.name = "Educational Campaign";
        this.reportObserver = reportObserver;
        this.image = "/campaign/educational.png";
    }

    @Override
    public void update() {
        String donationType = reportObserver.getType();

        if (donationType.equals("Educational")) {
            addTotalDonation(reportObserver.getDonationAmount());
        }
    }
}
