package com.charitable.report.core;

public abstract class Report {
    private int totalDonation;
    protected String name, image;
    protected ReportObserver reportObserver;

    public abstract void update();

    public int getTotalDonation() {
        return totalDonation;
    }

    public void addTotalDonation(int newDonation) {
        this.totalDonation += newDonation;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }
}
