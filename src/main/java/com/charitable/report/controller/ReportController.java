package com.charitable.report.controller;

import com.charitable.report.core.Report;
import com.charitable.report.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ReportController {

    @Autowired
    private ReportService reportService;

    @GetMapping("/all")
    public List<String> getReport(){
        List<String> reportOutput = new ArrayList<>();

        for (Report report : reportService.getReport()) {
            reportOutput.add("" +
                    "<img class=\"card-img-top\" src=\"" + report.getImage() + "\" alt=\"Card image cap\">\n" +
                    "<div class=\"card-body\">\n" +
                    "   <h5>" + report.getName() + "</h5>\n" +
                    "   <h4> Rp" + report.getTotalDonation() + "</h4>\n" +
                    "</div>");
        }

        return reportOutput;
    }

    @GetMapping("/notify/{type}")
    public void notifyReport(@PathVariable String type, @RequestParam int donationAmount) {
        reportService.updateReport(type, donationAmount);
    }

}
